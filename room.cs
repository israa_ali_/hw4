﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace attendanceManagement
{
    public partial class room : Form
    {
        public room()
        {
            InitializeComponent();
        }

        private void roomsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.roomsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.attManagementDataSet);

        }

        private void room_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'attManagementDataSet.rooms' table. You can move, or remove it, as needed.
            this.roomsTableAdapter.Fill(this.attManagementDataSet.rooms);

        }
    }
}
