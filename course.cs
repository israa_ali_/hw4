﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace attendanceManagement
{
    public partial class course : Form
    {
        public course()
        {
            InitializeComponent();
        }

        private void coursesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.coursesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.attManagementDataSet);

        }

        private void course_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'attManagementDataSet.courses' table. You can move, or remove it, as needed.
            this.coursesTableAdapter.Fill(this.attManagementDataSet.courses);

        }
    }
}
