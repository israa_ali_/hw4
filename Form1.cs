﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace attendanceManagement
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void attendancesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.attendancesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.attManagementDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'attManagementDataSet.courses' table. You can move, or remove it, as needed.
            this.coursesTableAdapter.Fill(this.attManagementDataSet.courses);
            // TODO: This line of code loads data into the 'attManagementDataSet.teachers' table. You can move, or remove it, as needed.
            this.teachersTableAdapter.Fill(this.attManagementDataSet.teachers);
            // TODO: This line of code loads data into the 'attManagementDataSet.rooms' table. You can move, or remove it, as needed.
            this.roomsTableAdapter.Fill(this.attManagementDataSet.rooms);
            // TODO: This line of code loads data into the 'attManagementDataSet.attendances' table. You can move, or remove it, as needed.
            this.attendancesTableAdapter.Fill(this.attManagementDataSet.attendances);

        }

        private void attendancesDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            new room().ShowDialog();
            this.roomsTableAdapter.Fill(this.attManagementDataSet.rooms);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            new teacher().ShowDialog();
            this.teachersTableAdapter.Fill(this.attManagementDataSet.teachers);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            new course().ShowDialog();
            this.coursesTableAdapter.Fill(this.attManagementDataSet.courses);

        }
    }
}
