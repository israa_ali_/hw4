﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace attendanceManagement
{
    public partial class teacher : Form
    {
        public teacher()
        {
            InitializeComponent();
        }

        private void teachersBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.teachersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.attManagementDataSet);

        }

        private void teacher_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'attManagementDataSet.teachers' table. You can move, or remove it, as needed.
            this.teachersTableAdapter.Fill(this.attManagementDataSet.teachers);

        }
    }
}
